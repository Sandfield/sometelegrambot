from models import Texts, LeadTexts

texts = Texts()
texts.greet_msg = 'Привет'
texts.back_btn = 'Назад'
texts.use_buttons_msg = 'Используй только кнопки'
texts.choose_good_msg = 'Выбери товар'
texts.choose_city_msg = 'Выбери город'
texts.choose_district_msg = 'Выбери район'
texts.choose_payment_msg = 'Выбери способ оплаты'
texts.done_btn = 'Готово'
texts.leads_btn = 'Лиды'
texts.new_order_msg = 'Новый заказ ID: {}\n' \
                      'Имя Фамилия: {} {}\n' \
                      'Username: {}\n' \
                      'Город: {}\n' \
                      'Район: {}\n' \
                      'Товар: {}\n' \
                      'Оплата: {}\n' \
                      'Код: {}'
texts.selected_good_msg = 'Выбрано: *{}*\n' \
                          'Коротко о товаре: *{}*\n' \
                          'Цена: *{}* грн.\n\n' \
                          'Выбирай район:'
l_texts = LeadTexts()
l_texts.wallet_btn = 'Кошелек'
l_texts.wallet_reply = 'Введи кошелек'
l_texts.withdraw_btn = 'Заказать выплату'
l_texts.withdraw_reply = 'Введи сумму'
l_texts.withdraw_incorrect_sum = 'Минимальная сумма - 500'
l_texts.withdraw_low_balance = 'Низкий баланс'
l_texts.how_to_btn = 'Как привлечь трафик?'
l_texts.how_to_reply = 'Вот так.'
l_texts.request_btn = 'Отправить лид на проверку'
l_texts.request_reply = 'Введи код (команду)'
l_texts.request_incorrect_sum = 'Некорректная сумма платежа'
l_texts.summary_msg = 'Ставка: *{}%*\n' \
                      'Всего лидов: *{}*\n' \
                      'Сегодня: *{}*\n' \
                      'В обработке: *{}*\n' \
                      'Одобрено: *{}*\n' \
                      'К выводу: *{}* грн.'

tax = 50
min_withdraw_sum = 500
