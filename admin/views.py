import base64
from threading import Thread

import flask
import telebot
from flask import Blueprint, redirect, url_for, request, render_template, flash
from flask_admin import Admin, BaseView, expose, AdminIndexView
from flask_admin.contrib.mongoengine import ModelView
from flask_login import LoginManager, current_user, login_user, logout_user
from telebot.apihelper import ApiException

import admin.methods as methods
import defaults
from admin.forms import LoginForm
from bot.bot_manager import BotManager
from models import User, Administrator, Texts, Mailing, District, Payment, City, BotProfile, Good, BotPhoto, LeadTexts, \
    LeadRequest, WithdrawRequest, Order

admin_blueprint = Blueprint('admin_bp', __name__)
login = LoginManager()

BOTS_INDEX_BREADCRUMBS = [{
    'name': '@',
    'url': '/admin/bots'
}]


@login.user_loader
def load_user(user_id):
    return Administrator.objects(id=user_id).first()


@admin_blueprint.route('/')
def index():
    return redirect(url_for('admin.index'))


def validate_login(user, form):
    if user is None:
        return False

    if user.password == form.password.data:
        return True
    else:
        return False


@admin_blueprint.route('/login', methods=['GET', 'POST'])
def handle_login():
    if current_user.is_authenticated:
        return redirect('/admin/bots')
    form = LoginForm()
    if form.validate_on_submit():
        user = Administrator.objects(username=form.username.data).first()

        if validate_login(user, form):
            login_user(user)
            flash('Logged in', category='success')
            return redirect(url_for('admin.index'))
        else:
            flash('Wrong username or password', category='danger')

    return render_template('login.html', form=form)


@admin_blueprint.route('/logout')
def handle_logout():
    logout_user()
    return redirect(url_for('admin.index'))


class MyIndexView(AdminIndexView):
    @expose('/')
    def index(self):
        return redirect('bots')


class BreadcrumbsView(ModelView):
    def get_breadcrumbs(self):
        return []

    def _handle_view(self, name, **kwargs):
        self._template_args.update(breadcrumbs=self.get_breadcrumbs())
        return super()._handle_view(name, **kwargs)


class SecureView(BreadcrumbsView):
    def is_accessible(self):
        return current_user.is_authenticated

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return redirect('/login')
        return super()._handle_view(name, **kwargs)


class RootView(SecureView):

    def _handle_view(self, name, **kwargs):
        menu_items = [{
            'caption': 'Боты',
            'url': '/admin/bots',
        }, {
            'caption': 'Пользователи',
            'url': '/admin/users',
        }, {
            'caption': 'Рассылка',
            'url': '/admin/mailing',
        }, {
            'caption': 'Лиды',
            'url': '/admin/lead-requests',
        }, {
            'caption': 'Заказы',
            'url': '/admin/orders',
        }, {
            'caption': 'Запросы на вывод',
            'url': '/admin/withdraw-requests',
        }, {
            'caption': 'Тексты (лиды)',
            'url': '/admin/lead-texts',
        }, {
            'caption': 'Админы',
            'url': '/admin/settings',
        }]
        for item in menu_items:
            item['active'] = item['caption'] == self.name
        self._template_args.update(menu_items=menu_items)
        return super()._handle_view(name, **kwargs)

    def get_breadcrumbs(self):
        return BOTS_INDEX_BREADCRUMBS


class BotModelView(BreadcrumbsView):

    def set_bot(self, bot_id):
        bot_profile = BotProfile.objects.get(id=bot_id)
        if bot_profile is not None:
            self._template_args.update(bot=bot_profile)

    @expose('/api/file/', methods=('POST',))
    def api_file_view(self, bot_id):
        self.set_bot(bot_id)
        return super().api_file_view()

    @expose('/delete/', methods=('POST',))
    def delete_view(self, bot_id):
        self.set_bot(bot_id)
        return super().delete_view()

    @expose('/edit/', methods=('GET', 'POST'))
    def edit_view(self, bot_id):
        self.set_bot(bot_id)
        return super().edit_view()

    @expose('/create', methods=('GET', 'POST'))
    def create_view(self, bot_id):
        self.set_bot(bot_id)
        return super().create_view()

    @expose('/')
    def index_view(self, bot_id):
        self.set_bot(bot_id)
        return super().index_view()

    def get_url(self, endpoint, **kwargs):
        kwargs.update(bot_id=str(self._template_args['bot'].id))
        return url_for(endpoint, **kwargs)

    def get_breadcrumbs(self):
        res = BOTS_INDEX_BREADCRUMBS + []
        bp = self._template_args.get('bot')
        if bp is not None:
            res.append({
                'name': bp.name,
                'url': url_for('bots/<bot_id>.index_view', bot_id=bp.id)
            })
        return res

    def get_custom_query(self, add_bot):
        query = super().get_query()
        if add_bot:
            query = query.filter(**{'bot_id': str(self._template_args['bot'].id)})
        return query

    def get_query(self):
        return self.get_custom_query(True)


class CityModelView(BotModelView):

    def set_city(self, city_id):
        city = City.objects.get(id=city_id)
        if city is not None:
            self._template_args.update(city=city)

    @expose('/api/file/')
    def api_file_view(self, bot_id, city_id):
        self.set_city(city_id)
        return super().api_file_view(bot_id)

    @expose('/delete/', methods=('POST',))
    def delete_view(self, bot_id, city_id):
        self.set_city(city_id)
        return super().delete_view(bot_id)

    @expose('/edit/', methods=('GET', 'POST'))
    def edit_view(self, bot_id, city_id):
        self.set_city(city_id)
        return super().edit_view(bot_id)

    @expose('/create', methods=('GET', 'POST'))
    def create_view(self, bot_id, city_id):
        self.set_city(city_id)
        self.form_args = dict(city_id=city_id)
        return super().create_view(bot_id)

    @expose('/')
    def index_view(self, bot_id, city_id):
        self.set_city(city_id)
        return super().index_view(bot_id)

    def get_url(self, endpoint, **kwargs):
        kwargs.update(city_id=str(self._template_args['city'].id))
        return super().get_url(endpoint, **kwargs)

    def get_breadcrumbs(self):
        res = super().get_breadcrumbs()
        bot = self._template_args.get('bot')
        city = self._template_args.get('city')
        if city is not None and bot is not None:
            res.append({
                'name': city.name,
                'url': '/admin/bots/%s/%s' % (str(bot.id), str(city.id))
            })
        return res

    def get_query(self):
        return super().get_custom_query(False).filter(**{'city_id': str(self._template_args['city'].id)})


class BotRootView(BotModelView):

    def _handle_view(self, name, **kwargs):
        res = super()._handle_view(name, **kwargs)
        bot = self._template_args.get('bot')
        if bot is not None:
            menu_items = [{
                'caption': 'Города',
                'url': '/admin/bots/%s' % bot.id,
            }, {
                'caption': 'Варианты оплаты',
                'url': '/admin/bots/%s/payments' % bot.id,
            }, {
                'caption': 'Тексты',
                'url': '/admin/bots/%s/texts' % bot.id,
            }]
            for item in menu_items:
                item['active'] = item['caption'] == self.name
            self._template_args.update(menu_items=menu_items)
        return res


class CityRootView(CityModelView):

    def _handle_view(self, name, **kwargs):
        res = super()._handle_view(name, **kwargs)
        bot = self._template_args.get('bot')
        city = self._template_args.get('city')
        if bot is not None and city is not None:
            menu_items = [{
                'caption': 'Продукты',
                'url': '/admin/bots/%s/%s/goods' % (bot.id, city.id),
            }, {
                'caption': 'Районы',
                'url': '/admin/bots/%s/%s/districts' % (bot.id, city.id),
            }]
            for item in menu_items:
                item['active'] = item['caption'] == self.name
            self._template_args.update(menu_items=menu_items)
        return res


class MyUserView(RootView):
    list_template = 'user_list.html'
    column_searchable_list = ['username', 'first_name', 'last_name']
    can_set_page_size = True
    column_filters = ['username', 'first_name', 'last_name']

    @expose('<user_id>/send_message', methods=['GET', 'POST'])
    def send_message(self, user_id):
        user = User.objects(id=user_id).first()
        if user is None:
            return '', 404
        if request.form:
            if user.bot:
                body = request.form.get('body')
                bot = BotManager().get(str(user.bot.id))
                if bot is None:
                    flash('Бот не активен', category='error')
                elif body:
                    bot.send_message(user.user_id, body, parse_mode='markdown')
                    flash('Отправлено', category='success')
            return redirect(url_for('users.send_message', user_id=user_id))
        return self.render('user_send_message.html', user=user)


class MyTextsView(BotRootView):
    create_template = 'texts_create.html'
    edit_template = 'texts_edit.html'
    column_list = ['greet_msg', 'choose_good_msg', 'choose_city_msg', 'choose_district_msg', 'choose_payment_msg']

    @expose('/create', methods=('GET', 'POST'))
    def create_view(self, bot_id):
        self._template_args.update(texts=defaults.texts.to_json())
        return super().create_view(bot_id)


class MyMailingView(RootView):
    list_template = 'mailing_list.html'

    @expose('<mailing_id>/send', methods=['GET', 'POST'])
    def send(self, mailing_id):
        mailing = Mailing.objects(id=mailing_id).first()
        if mailing is None:
            return '', 404
        if request.form:
            bots = [dict(bot_id=bot_id, bot=BotManager().get(bot_id)) for bot_id in request.form.getlist('bots')]
            bots = [bot for bot in bots if bot['bot'] is not None]
            if bots is None or len(bots) == 0:
                flash('Нет активных ботов', category='error')
            else:
                for bot in bots:
                    Thread(target=methods.send_messages, args=(bot['bot_id'], bot['bot'], mailing,)).start()
                flash(f'Отправлено ({len(bots)})', category='success')
                return redirect(url_for('mailing.send', mailing_id=mailing_id))
        return self.render('mailing_send.html', mailing=mailing, bots=BotProfile.objects(active=True))


class MyBotProfileView(RootView):
    list_template = 'bot_list.html'
    create_template = 'bot_create.html'
    edit_template = 'bot_edit.html'
    column_filters = ['name', 't_id']
    column_list = ['active', 'photo_file_id', 'name', 't_id']
    column_labels = dict(active='Активен', photo_file_id='', name='Name', t_id='Telegram ID')

    @expose('fetch')
    def fetch(self):
        try:
            token = request.args['token']
            fb = telebot.TeleBot(token)
            me = fb.get_me()
            t_id = str(me.id)
            profile = dict(name=me.username, t_id=t_id)
            photos = fb.get_user_profile_photos(me.id, limit=1).photos
            if len(photos) > 0:
                file_id = photos[0][0].file_id
                profile['photo_file_id'] = file_id
                photo = BotPhoto.objects(t_bot_id=t_id, file_id=file_id).first()
                if photo is None:
                    img = fb.download_file(fb.get_file(file_id).file_path)
                    photo = BotPhoto(t_bot_id=t_id, file_id=file_id, data=img)
                    BotPhoto.save(photo)
                profile['photo'] = base64.b64encode(photo.data).decode('ascii')
        except ApiException:
            return dict(error='WRONG_TOKEN'), 400
        return profile

    @expose('<bot_id>/active', methods=['POST'])
    def set_active(self, bot_id):
        bot = BotProfile.objects(id=bot_id).first()
        if bot is None:
            return '', 404
        bot.active = request.args['active'] == 'true'
        bot.save()
        if bot.active and not BotManager().is_running(bot_id):
            BotManager().run(bot)
        elif not bot.active and BotManager().is_running(bot_id):
            BotManager().stop(bot_id)
        return {'active': bot.active}

    @expose('<bot_id>/photo')
    def bot_photo(self, bot_id):
        bot = BotProfile.objects(id=bot_id).first()
        if bot is None or bot.photo_file_id is None or len(bot.photo_file_id) == 0:
            return '', 404
        photo = BotPhoto.objects(t_bot_id=bot.t_id, file_id=bot.photo_file_id).first()
        if photo is None:
            return '', 404
        resp = flask.Response(photo.data)
        resp.headers['Content-Type'] = 'image/png'
        return resp

    @expose('/delete/', methods=('POST',))
    def delete_view(self):
        bot_id = request.form.get('id')
        if BotManager().is_running(bot_id):
            BotManager().stop(bot_id)
        return super().delete_view()


class MyCityView(BotRootView):
    list_template = 'city_list.html'
    create_template = 'city_create.html'
    edit_template = 'city_edit.html'
    column_filters = ['name']
    column_list = ['name']


class MyPaymentView(BotRootView):
    create_template = 'payment_create.html'
    edit_template = 'payment_edit.html'
    column_filters = ['payment_name', 'answer_text']
    column_list = ['payment_name', 'answer_text']


class MyDistrictView(CityRootView):
    create_template = 'district_create.html'
    edit_template = 'district_edit.html'
    column_filters = ['name']
    column_list = ['name']


class MyGoodsView(CityRootView):
    create_template = 'good_create.html'
    edit_template = 'good_edit.html'
    # column_filters = ['name']
    # column_list = ['name']


class MyCityOneView(BaseView):
    @expose('/')
    def index_view(self, bot_id, city_id):
        return redirect('/admin/bots/%s/%s/goods' % (bot_id, city_id))


class MyLeadTextsView(RootView):
    create_template = 'lead_texts_create.html'

    @expose('/create', methods=('GET', 'POST'))
    def create_view(self):
        self._template_args.update(texts=defaults.l_texts.to_json())
        return super().create_view()


class LeadRequestsView(RootView):
    can_create = False
    can_edit = False
    column_filters = ['approved', 'user']
    column_list = ['approved', 'user', 'code', 'sum', 'date']
    list_template = 'lead_request_list.html'

    @expose('<lr_id>/approve', methods=['POST'])
    def approve(self, lr_id):
        lr = LeadRequest.objects(id=lr_id).first()
        if lr is None or lr.approved:
            return '', 404
        lr.approved = True
        lr.save()
        if not lr.user.balance:
            lr.user.balance = 0
        lr.user.balance += lr.sum
        lr.user.save()
        return {'approved': lr.approved}


class WithdrawRequestsView(RootView):
    can_create = False
    can_edit = False
    column_filters = ['done', 'user']
    column_list = ['done', 'user', 'sum']
    list_template = 'withdraw_request_list.html'

    @expose('<wr_id>/done', methods=['POST'])
    def set_done(self, wr_id):
        wr = WithdrawRequest.objects(id=wr_id).first()
        if wr is None or wr.done:
            return '', 404
        if not wr.user.balance or wr.user.balance < wr.sum:
            return {'error': 'LOW_BALANCE'}, 400
        wr.done = True
        wr.save()
        wr.user.balance -= wr.sum
        wr.user.save()
        return {'done': wr.done}


class OrdersView(RootView):
    can_create = False
    can_edit = False


admin = Admin(template_mode='bootstrap3', index_view=MyIndexView())

admin.add_view(MyUserView(User, name='Пользователи', endpoint='users'))
admin.add_view(MyMailingView(Mailing, name='Рассылка', endpoint='mailing'))
admin.add_view(LeadRequestsView(LeadRequest, name='Лиды', endpoint='lead-requests'))
admin.add_view(WithdrawRequestsView(WithdrawRequest, name='Запросы на вывод', endpoint='withdraw-requests'))
admin.add_view(OrdersView(Order, name='Заказы', endpoint='orders'))
admin.add_view(MyLeadTextsView(LeadTexts, name='Тексты (лиды)', endpoint='lead-texts'))
admin.add_view(RootView(Administrator, name='Админы', endpoint='settings'))

admin.add_view(MyBotProfileView(BotProfile, name='Боты', endpoint='bots'))
admin.add_view(MyPaymentView(Payment, name='Варианты оплаты', endpoint='bots/<bot_id>/payments'))
admin.add_view(MyCityView(City, name='Города', endpoint='bots/<bot_id>'))
admin.add_view(MyTextsView(Texts, name='Тексты', endpoint='bots/<bot_id>/texts'))
admin.add_view(MyCityOneView(City, endpoint='bots/<bot_id>/<city_id>'))
admin.add_view(MyGoodsView(Good, name='Продукты', endpoint='bots/<bot_id>/<city_id>/goods'))
admin.add_view(MyDistrictView(District, name='Районы', endpoint='bots/<bot_id>/<city_id>/districts'))
